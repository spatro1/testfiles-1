<ac:layout>
    <ac:layout-section ac:breakout-mode="default" ac:type="fixed-width">
        <ac:layout-cell>
            
            <ac:structured-macro ac:macro-id="ccde919c-0402-4daf-b433-803272e34735" ac:name="tip" ac:schema-version="1">
                <ac:rich-text-body>
                        <h3><strong>Welcome to your new knowledge base space</strong></h3>
                        <p>A knowledge base is a shared resource that your whole team can use and contribute to.</p>
                        <p>Use it to build a wiki, create content to deflect help-desk requests, or for anything else where you want to quickly create lots of pages without worrying about organization and structure.</p>
                        <p><strong>To start, you might want to:</strong></p>
                        <ul>
                            <li><p><strong>Customise this overview </strong>using the <strong>edit icon </strong>at the top right of this page.</p></li>
                            <li><p><strong>Create a new page </strong>by clicking the <strong>+ </strong>in the space sidebar.</p></li>
                        </ul>
                        <p><strong>Tip: </strong>Make it easier for your team to find what they're looking for by adding <a href="https://confluence.atlassian.com/confcloud/use-labels-to-organize-your-content-724764874.html">labels </a>to your pages so they appear when you <strong>Browse by topic </strong>below
                        </p>
                </ac:rich-text-body>
            </ac:structured-macro>
            <p />
            <h2 style="text-align: center;">Search this space</h2>
            <p style="text-align: center;"><ac:structured-macro ac:macro-id="765149bd-15e8-4704-a6f9-73b0a1d79b50" ac:name="livesearch" ac:schema-version="1" /></p>
            <hr />
        </ac:layout-cell>
    </ac:layout-section>
    
    <ac:layout-section ac:breakout-mode="default" ac:type="two_equal">
        <ac:layout-cell>
            <h2>Frequently used articles</h2>
            <ul><li><p>Add links to popular how-to and troubleshooting articles.</p></li><li><p>Highlight important documentation.</p></li></ul>
        </ac:layout-cell>
        
        <ac:layout-cell>
            <h2>Need more help?</h2>
            <ul> <li><p>Link to resources such as your service desk, questions and answers or a forum.</p></li>
                <li>
                    <p>List contacts for getting additional help.</p>
                </li>
            </ul>
        </ac:layout-cell>
    </ac:layout-section>
    
    <ac:layout-section ac:breakout-mode="default" ac:type="fixed-width">
        <ac:layout-cell>
            <hr />
            <h2>Browse by topic</h2>
            <ac:structured-macro ac:macro-id="72745541-a349-45d0-8ac0-92b57f265ac0" ac:name="listlabels" ac:schema-version="1" data-layout="default" />
            <p />

            <ac:structured-macro ac:macro-id="ca94a9dd-a41f-44f3-bd46-9d1f9985d6a1" ac:name="info" ac:schema-version="1">
                <ac:rich-text-body>
                    <h6>NEED INSPIRATION?</h6>
                    <ul>
                        <li>
                        <p>Check out this guide on how to <a href="https://confluence.atlassian.com/confcloud/use-confluence-as-a-knowledge-base-724765471.html">use Confluence as a Knowledge base</a></p></li>
                        <li>
                            <p>Follow <a href="https://www.atlassian.com/blog/confluence/twitters-5-tips-successful-knowledge-base">Twitter's 5 tips for a successful knowledge base</a></p>
                        </li>
                    </ul>
                </ac:rich-text-body>
            </ac:structured-macro>
            <p />
        </ac:layout-cell>
    </ac:layout-section>
</ac:layout>